import React, { Component } from 'react';
import Expo from 'expo';
import {
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Button,
  ImageBackground,
  AppRegistry

} from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { StackNavigator } from 'react-navigation';
import { width, height, totalSize } from 'react-native-dimension';

const styles = StyleSheet.create({
  title: {
    color: "white",
    fontFamily: 'Arial',
    fontWeight: 'bold',
    textAlign  : 'center',
    marginTop: height(40),
    fontSize: totalSize(4),
    backgroundColor: 'rgba(0,0,0,0)'
  },

  container: {
  flex: 1,
  width: width(100),
  height: height(100),
}
})


export default class HomeScreen extends Component {

  state= {
    loginCheck: null,
    username: '',
    password: '',
  }

  static navigationOptions = {
    title: 'Login',
    header: null,
  }


  _onPressButton(database, navigate) { //on press login
      if(this.state.username in database){ //check if username in database
        if(database[this.state.username] === this.state.password){ //if yes, check if password matches
          this.setState({
            loginCheck: true
          })
          navigate("Main", {screen: "MainScreen"})
        }

        else{ //password does not match
          this.setState({
            loginCheck: false
          })
        }
      }

      else{ //username not in storage
        this.setState({
          loginCheck: false
        })
      }
      return this.state.loginCheck
  };


  render() {

    StatusBar.setHidden(true);
    var database = require('./database.json');
    const {navigate} = this.props.navigation

    return (
      <ImageBackground source ={require('./assets/images/loginBackground.png')} style={styles.container}>

        <Text style={styles.title}>Good Samaritan</Text>

        <TextInput
          style={{
            height:height(5),
            padding: 5,
            marginLeft: width(15),
            borderColor:"rgba(0, 0, 0, 0.1)",
            borderWidth: 1,
            marginTop:height(2),
            width: width(70),
            backgroundColor: 'white',
          }}
          placeholder= "Username or Email"
          maxLength = {25}
          autoCapitalize = 'none'
          returnKeyLabel = {'next'}
          onChangeText= {(text) => this.setState({username: text})}
        />

        <TextInput
          style={{
            height:height(5),
            padding: 5,
            marginLeft: width(15),
            borderColor:"rgba(0, 0, 0, 0.1)",
            borderWidth: 1,
            marginTop:height(2),
            width: width(70),
            backgroundColor: 'white',
          }}
          placeholder = "Password"
          maxLength = {20}
          secureTextEntry = {true}
          returnKeyLabel={"next"}
          onChangeText= {(text) => this.setState({password:text})}
        />

        <View style={{backgroundColor: 'rgba(0,0,0,0)', marginTop: height(5)}}>
          <TouchableOpacity onPress={() => this._onPressButton(database, navigate)}>
              <Text style={{
                color: 'white',
                textAlign: 'center',
                fontFamily: 'Arial',
                height: height(8),
                fontSize: totalSize(2.5),
              }}>Login</Text>
          </TouchableOpacity>

          <Text style={{ // invalid login pop-up
            color: (this.state.loginCheck == false ? 'white': 'rgba(0,0,0,0)'),
            fontFamily: 'Verdana',
            textAlign: 'center',
            marginTop: height(12.48),//21.5
            height: height(8),
            padding: height(2.4),
            fontSize: totalSize(2),
            backgroundColor: (this.state.loginCheck == false ? 'rgba(255,80,80,0.7)': 'rgba(0,0,0,0)')
            }}> Invalid Username, Email, or Password </Text>
          </View>
      </ImageBackground>
    )
  }
}
